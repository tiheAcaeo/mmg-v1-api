<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::table('zones', function(Blueprint $table) {
            $table->renameColumn('user_id', 'garden_id');
        });
    }


    public function down()
    {
        Schema::table('zones', function(Blueprint $table) {
            $table->renameColumn('garden_id', 'user_id');
        });
    }
};
