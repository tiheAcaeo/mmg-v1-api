<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\TaskDefinitionRequest;

class StoreTaskDefinitionTypeRequest extends TaskDefinitionRequest
{
    use ApiRequestTrait;
}
