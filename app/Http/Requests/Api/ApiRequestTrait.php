<?php
namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

trait ApiRequestTrait {
    public function failedValidation(Validator $validator){
        throw new HttpResponseException(response()->json([
            'success'   => false,
            'message'   => 'Validation errors',
            'data'      => $validator->errors()
        ]));

    }    
    
    /**
    * Determine if the user is authorized to make this request.
    */
   public function authorize(): bool
   {
       return true; // FIXME : backpack_auth()->check() doew not

   }
}