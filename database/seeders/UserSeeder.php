<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@pangaia.app',
            'password' => 'Passwordpassword42',
            'current_team_id' => '1'

        ]);

        DB::table('users')->insert([
            'name' => 'demo',
            'email' => 'demo@pangaia.app',
            'password' => 'Passwordpassword42',
            'current_team_id' => '2'

        ]);

        DB::table('users')->insert([
            'name' => 'user',
            'email' => 'user@pangaia.app',
            'password' => 'Passwordpassword42',
            'current_team_id' => '3'

        ]);
    }
}
