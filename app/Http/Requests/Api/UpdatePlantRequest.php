<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\PlantRequest;

class UpdatePlantRequest extends PlantRequest
{
     use ApiRequestTrait;
}
